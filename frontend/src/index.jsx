import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';

import Spinner from './components/Spinner';

const App = lazy(() => import('./App'));

import AppProvider from './store/provider';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <AppProvider>
      <Suspense fallback={<Spinner />}>
        <App />
      </Suspense>
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
