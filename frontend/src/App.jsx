import React, { useContext, useEffect } from 'react';

import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';

import AppContext from './store/context';

import styles from './App.module.css';

export default function App() {

  const {
    state: { data, search },
    dispatch
  } = useContext(AppContext);

  async function getData(search) {
    const response = await fetch(`/data/${search}`);
    const data = await response.json();
    dispatch({ type: 'update', payload: data });
  }

  useEffect(() => {
    getData(search);
  }, [search]);

  return (
    <div className={styles.app}>
      <Header />
      <Main data={data} />
      <Footer />
    </div>
  );
}
