import { initialState, reducer } from './reducer';

describe('Initial state', () => {

  test('Checks initial state', () => {
    const test = { data: [], search: '', activeTab: 1 };
    expect(test).toMatchObject(initialState);
  });

});

describe('Reducer', () => {

  test('Checks add action', () => {

    const action = {
      type: 'add',
      payload: {
        name: 'Andy',
        email: 'andy@gmail.com',
        company: 'EV'
      }
    };

    const result = reducer(initialState, action);
    expect(result.data.length).toEqual(1);
    expect(result.data[0]).toEqual(action.payload);

  });

  test('Checks activeTab action', () => {

    const action = {
      type: 'activeTab',
      payload: 2
    };

    const result = reducer(initialState, action);
    expect(result.activeTab).toEqual(2);

  });

  test('Checks add action', () => {

    const action = {
      type: 'update',
      payload: [{
        name: 'Andy',
        email: 'andy@gmail.com',
        company: 'EV'
      }]
    };

    const result = reducer(initialState, action);
    expect(result.data.length).toEqual(1);
    expect(result.data).toEqual(action.payload);

  });

  test('Checks search action', () => {

    const action = {
      type: 'search',
      payload: 'bob'
    };

    const result = reducer(initialState, action);
    expect(result.search).toEqual('bob');

  });

  test('Checks default action', () => {

    const result = reducer(initialState);
    expect(result).toEqual(initialState);

  });

});
