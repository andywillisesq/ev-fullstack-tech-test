export const initialState = {
  data: [],
  search: '',
  activeTab: 1
};

export function reducer(state = {}, action = {}) {

  const { type, payload } = action;

  switch (type) {

    case 'activeTab': {
      return { ...state, activeTab: payload };
    }

    case 'add': {
      return {
        ...state,
        activeTab: 1,
        data: [ ...state.data, payload ]
      };
    }

    case 'update': {
      return { ...state, data: payload };
    }

    case 'search': {
      return { ...state, search: payload };
    }

    default: {
      return state;
    }

  }

}
