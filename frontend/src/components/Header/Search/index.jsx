import React, { useContext } from 'react';

import Input from '../../Form/Input';

import AppContext from '../../../store/context';

export default function Search() {

  const {
    state: { search },
    dispatch
  } = useContext(AppContext);

  function handleSearch(e) {
    dispatch({
      type: 'search',
      payload: e.target.value
    });
  }

  return (
    <Input
      type="text"
      name="search"
      htmlFor="search"
      placeholder="Enter client name"
      label="Client search"
      value={search}
      handleChange={handleSearch}
    />
  );

}
