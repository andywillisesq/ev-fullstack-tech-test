import React from 'react';
import { render, screen } from '@testing-library/react';

import AppProvider from '../../../store/provider';

import Search from './index';

describe('Footer', () => {
  test('Renders Footer component', () => {
    render(
      <AppProvider>
        <Search />
      </AppProvider>
    );
    expect(screen.getByLabelText('Client search'));
  });
});
