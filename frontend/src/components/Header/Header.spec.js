import React from 'react';
import { render } from '@testing-library/react';

import AppProvider from '../../store/provider';

import Header from './index';

const wrapper = (
  <AppProvider>
    <Header />
  </AppProvider>
);

describe('Header', () => {
  test('Renders Header component', () => {
    render(wrapper);
  });
});
