import React from 'react';

import Search from './Search';

import styles from './index.module.css';

export default function Header() {

  return (
    <header className={styles.header}>
      <Search />
    </header>
  );
}
