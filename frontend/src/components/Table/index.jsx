import React from 'react';

import HeadingRow from './HeadingRow';
import Row from './Row';

import styles from './index.module.css';

function getHeadings(data) {
  return <HeadingRow data={Object.keys(data[0])} />;
}

function getRows(data) {
  return data.map(obj => {
    const { id } = obj;
    return <Row key={id} data={obj} />;
  });
}

export default function Table({ data = [] }) {

  if (!data.length) {
    return (
      <div className={styles.nodata}>No client data available</div>
    );
  }

  return (
    <table className={styles.table}>
      <tbody>
        {getHeadings(data)}
        {getRows(data)}
      </tbody>
    </table>
  );

}
