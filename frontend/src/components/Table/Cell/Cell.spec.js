import React from 'react';
import { render } from '@testing-library/react';

import Cell from './index';

const tr = document.createElement('tr');

describe('Cell', () => {
  test('Renders Cell component', () => {
    render(<Cell />, {
      container: document.body.appendChild(tr)
    });
  });
});
