import React from 'react';
import classNames from 'classnames';

import styles from './index.module.css';

export default function Cell({ type = 'data', value = '' }) {

  const cn = classNames([
    styles.cell,
    type === 'data' && styles.data
  ]);

  return <td className={cn}>{value}</td>;

}
