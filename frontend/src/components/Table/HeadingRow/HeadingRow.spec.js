import React from 'react';
import { render } from '@testing-library/react';

import HeadingRow from './index';

const tbody = document.createElement('tbody');

describe('HeadingRow', () => {
  test('Renders HeadingRow component', () => {
    render(<HeadingRow />, {
      container: document.body.appendChild(tbody)
    });
  });
});
