import React from 'react';

import Cell from '../Cell';

import styles from './index.module.css';

export default function HeadingRow({ data = [] }) {
  return (
    <tr className={styles.headingRow}>
      {data.map(value => {
        return <Cell type="heading" key={value} value={value} />;
      })}
    </tr>
  );
}
