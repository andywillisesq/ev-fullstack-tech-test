import React from 'react';
import { render, screen } from '@testing-library/react';

import Row from './index';

const table = document.createElement('table');
const tbody = document.createElement('tbody');
table.appendChild(tbody);

describe('Row', () => {

  test('Renders Row component', () => {

    const data = {
      id: 1,
      name: 'Andy',
      email: 'andy@gmail.com',
      company: 'EV'
    };

    render(<Row data={data} />, {
      container: document.body.appendChild(tbody)
    });

    expect(screen.getByText('Andy'));
    expect(screen.getByText('andy@gmail.com'));
    expect(screen.getByText('EV'));

  });

});
