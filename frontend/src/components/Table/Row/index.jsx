import React from 'react';

import Cell from '../Cell';

import formatDate from '../../../lib/formatDate';

import styles from './index.module.css';

export default function Row({ data = [] }) {

  const { id } = data;

  return (
    <tr key={id} className={styles.row}>
      {Object.entries(data).map(([ key, value ], i) => {

        const cellKey = `${id}-${i}`;

        const formattedValue = key === 'createdDate'
          ? formatDate(value)
          : value;

        return <Cell key={cellKey} value={formattedValue} />;

      })}
    </tr>
  );

}
