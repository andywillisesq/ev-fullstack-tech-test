import React from 'react';
import { render } from '@testing-library/react';

import Table from './index';

describe('Table', () => {

  test('Renders Table component', () => {

    const data = [{
      id: 1,
      name: 'Andy',
      email: 'andy@gmail.com',
      company: 'EV'
    }];

    render(<Table data={data} />);

  });

});
