import React from 'react';

import Tab from './Tab';

import styles from './index.module.css';

import tabConfig from '../../config/tabs';

export default function TabContainer({ activeTab, handleTab }) {
  return (
    <div className={styles.tabContainer}>
      {tabConfig.map(tab => {
        const { id, text } = tab;
        return (
          <Tab
            key={id}
            id={id}
            active={id === activeTab}
            text={text}
            handleTab={handleTab}
          />
        );
      })}
    </div>
  );
}
