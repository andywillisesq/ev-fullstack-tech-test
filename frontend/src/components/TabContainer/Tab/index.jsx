import React from 'react';
import classNames from 'classnames';

import styles from './index.module.css';

export default function Tab(props) {

  const { id, active, text, handleTab } = props;

  const cn = classNames([
    styles.tab,
    active && styles.active
  ]);

  return (
    <button
      data-id={id}
      className={cn}
      type="button"
      onClick={handleTab}
    >{text}
    </button>
  );

}
