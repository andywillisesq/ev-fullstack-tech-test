import React from 'react';

import styles from './index.module.css';

export default function ContentContainer({ children }) {
  return (
    <div className={styles.contentConteiner}>
      {children}
    </div>
  );
}
