import React from 'react';
import { render, screen } from '@testing-library/react';

import AppProvider from '../../store/provider';

import Form from './index';

const wrapper = (
  <AppProvider>
    <Form />
  </AppProvider>
);

describe('Form', () => {

  test('Renders Form component', () => {
    render(wrapper);
  });

  test('Renders Form component - fieldset', () => {
    render(wrapper);
    expect(screen.getByText('New client details'));
  });

  test('Renders Form component - button', () => {
    render(wrapper);
    expect(screen.getByText('Add new client'));
  });

});
