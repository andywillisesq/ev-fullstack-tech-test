import React from 'react';

import styles from './index.module.css';

export default function Input(props) {

  const {
    name,
    type,
    label,
    required,
    placeholder = '',
    value,
    htmlFor,
    handleChange
  } = props;

  return (
    <div className={styles.inputContainer}>
      <label htmlFor={htmlFor} className={styles.label}>
        {label}{required && ' *'}&nbsp;
      </label>
      <input
        id={htmlFor}
        className={styles.input}
        required={required}
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={handleChange}
      />
    </div>
  );

}
