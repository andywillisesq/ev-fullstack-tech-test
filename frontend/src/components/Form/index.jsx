import React, { useContext, useRef, useState } from 'react';

import Input from './Input';

import AppContext from '../../store/context';

import styles from './index.module.css';

import formConfig from '../../config/form';

function initialiseState() {
  return formConfig.reduce((acc, { name }) => {
    return { ...acc, [name]: '' };
  }, {});
}

export default function Form() {

  const { dispatch } = useContext(AppContext);

  const [ form, setForm ] = useState(initialiseState(formConfig));
  const [ isValid, setIsValid ] = useState(false);

  const formRef = useRef();

  function handleChange(e) {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
    setIsValid(formRef.current.checkValidity());
  }

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const response = await fetch('/add', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(form)
      });
      const data = await response.json();
      dispatch({ type: 'add', payload: data });
    } catch (err) {
      console.log(`There was an error: ${err}`);
    }
  }

  return (
    <form
      ref={formRef}
      className={styles.form}
      onSubmit={handleSubmit}
    >
      <fieldset>
        <legend>New client details</legend>
        {formConfig.map(input => {
          const { name, type, label } = input;
          return (
            <Input
              key={name}
              required
              type={type}
              name={name}
              htmlFor={name}
              label={label}
              value={form[name]}
              handleChange={handleChange}
            />
          );
        })}
      </fieldset>
      <button
        disabled={!isValid}
        type="submit"
      >Add new client
      </button>
    </form>
  );
}
