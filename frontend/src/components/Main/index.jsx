import React, { useContext } from 'react';

import Container from '../Container';
import ContentContainer from '../ContentContainer';
import TabContainer from '../TabContainer';
import Table from '../Table';
import Form from '../Form';

import AppContext from '../../store/context';

import styles from './index.module.css';

function getContent(activeTab, data) {
  switch (activeTab) {
    case 1: return <Table data={data} />;
    case 2: return <Form />;
  }
}

export default function Main({ data }) {

  const {
    state: { activeTab },
    dispatch
  } = useContext(AppContext);

  function handleTab(e) {
    const { id } = e.target.dataset;
    dispatch({ type: 'activeTab', payload: +id });
  }

  return (
    <main className={styles.main}>
      <Container>
        <TabContainer
          activeTab={activeTab}
          handleTab={handleTab}
        />
        <ContentContainer>
          {getContent(activeTab, data)}
        </ContentContainer>
      </Container>
    </main>
  );

}
