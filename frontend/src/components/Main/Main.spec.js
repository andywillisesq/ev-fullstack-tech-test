import React from 'react';
import { render, screen } from '@testing-library/react';

import AppProvider from '../../store/provider';

import Main from './index';

describe('Main', () => {

  const wrapper = (
    <AppProvider>
      <Main />
    </AppProvider>
  );

  test('Renders Main component', () => {
    render(wrapper);
  });

  test('Renders Main component tabs', () => {
    render(wrapper);
    expect(screen.getByText('Add new client'));
    expect(screen.getByText('View client list'));
  });

  test('Renders Main component table - no data', () => {
    render(wrapper);
    expect(screen.getByText('No client data available'));
  });

});
