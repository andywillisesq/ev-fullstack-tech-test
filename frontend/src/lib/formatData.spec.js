import formatDate from './formatDate';

describe('formatDate', () => {
  test('Returns a formatted date', () => {
    const date = '2021-11-27T16:34:33.015Z';
    expect(formatDate(date)).toEqual('27/11/2021');
  });
});
