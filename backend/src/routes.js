const fsp = require('fs/promises');

function addData(data) {

  return function (req, res) {

    const client = {
      id: data.length + 1,
      ...req.body,
      createdDate: new Date(),
    };

    data.push(client);

    const path = `${__dirname}/../data/data.json`;
    const json = JSON.stringify(data, null, 2);

    try {
      fsp.writeFile(path, json, 'utf8');
    } catch (err) {
      console.log(err);
    }

    res.send(client);

  };

}

function getData(data) {

  return function (req, res) {
    const { name } = req.params;
    if (!name) {
      res.send(JSON.stringify(data));
    } else {
      const filtered = data.filter(obj => {
        return obj.name.toLowerCase().startsWith(name.toLowerCase());
      });
      res.send(JSON.stringify(filtered));
    }
  };

}

module.exports = { addData, getData };
