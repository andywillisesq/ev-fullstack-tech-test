const express = require('express');
const bodyParser = require('body-parser');

const data = require('../data/data.json');
const { addData, getData } = require('./routes');

const app = express();
const port = process.env.PORT || 4000;

app.use(bodyParser.json());

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

app.get('/data/:name?', getData(data));
app.post('/add', addData(data));
