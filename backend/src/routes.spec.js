const request = require('supertest');
const express = require('express');

const data = require('../data/data.json');
const { getData } = require('./routes');

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use('/data/:name?', getData(data));

describe('getData', () => {

  test('getData all route works', () => {

    request(app)
      .get('/data')
      .expect('Content-Type', 'text/json; charset=utf-8')
      .expect(data)
      .expect(200);

  });

  test('getData with name route works', () => {

    const data = '{"id": 2,"name": "Sue","email": "sue@gmail.com","company": "Our Price","createdDate": "2021-09-27T16:34:33.015Z"}';

    request(app)
      .get('/data/sue')
      .expect('Content-Type', 'text/json; charset=utf-8')
      .expect(data)
      .expect(200);

  });

});

describe('addData', () => {
  test('addData with name route works', () => {

    const data = '{"name": "Dave","email": "dave@gmail.com","company": "WH Smith"}';
    const result = '{"id": 4, "name": "Dave","email": "dave@gmail.com","company": "WH Smith", "createdDate": "2021-11-27T16:34:33.015Z"}';    

    request(app)
      .post('/add')
      .send(data)
      .expect('Content-Type', 'text/json; charset=utf-8')
      .expect(result)
      .expect(200);

  });

});
