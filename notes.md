# Andy Willis EV test

## UI design

In the interview you mentioned that you use MUI for interface design. I haven't looked at it in a couple of years. We reviewed it for Oxhey Hall but decided against it in favour of React Strap. Given that it would have taken me three hours to just pick my way through the MUI documentation I decided to quickly roll my own small bespoke components with CSS modules.

`Heading` contains the `Search` component.

`Form` contains the `Input` component.

`Table` contains the `Row`, `HeadingRow`, and `Cell` components.

While I did consider a responsive layout, and I do use `flex` a lot in my CSS modules, the app itself isn't responsive. The key decision behind this was how to accurately represent large tables of data (more than 5 columns at least) on smaller devices which I thought went beyond the remit of this exercise. So this app is desktop only. (It was something that came up designing a UI for Oxhey Hall so I appreciate how difficult it is to get it right.)

## State management

I've used React context to maintain state for the data, search string, and the current active tab. `Form` handles its own state, updates the data on the server, and then updates the data state with the new client object.

The initial data is loaded from `/data` in `App.jsx` and passed down to `Main` so the table can be rendered.

`App.jsx` also listens for changes to the search state, and pulls data matching that search string (client name) from the server, and updates the local data state. Ideally this search would be throttled but I haven't added that functionality to this example.

## Testing

I added the [`react-testing-library`](https://testing-library.com/docs/react-testing-library/intro) to help create the unit tests.

The front-end code is fully tested (over 80% coverage). The back-end has a test for the routes, but nothing more than that.

## Data persistence

I haven't used a database for this small example. There is some initial data held in `/data/data.json` on the server which is loaded when the server starts. Changes to the data (from the `add` route) are subsequently saved back to that file. It's persistent, but clearly not scalable.

I have a separate branch where I'm investigating MongoDB integration but that's not been merged in.